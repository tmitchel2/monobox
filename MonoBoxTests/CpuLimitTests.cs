﻿using System;
using System.Diagnostics;
using System.Threading;
using MonoBox;
using NUnit.Framework;

namespace MonoBoxTests
{
    [TestFixture]
    public class CpuLimitTests
    {
        [Test, Explicit]
        public void Test45PercentCpuLimit()
        {
            var startInfo = new ProcessStartInfo("MonoBoxTestApp.exe")
                                {
                                    UseShellExecute = false
                                };

            var process = Process.Start(startInfo);
            using(var monobox = DefaultMonoBoxFactory.Instance.CreateMonoBox("MonoBoxTest"))
            {
                monobox.CpuLimit = 45;
                monobox.AddProcess(process);
                Thread.Sleep(TimeSpan.FromSeconds(30));
            }
        }
    }
}
