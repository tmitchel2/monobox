﻿using System;
using System.Diagnostics;

namespace MonoBox
{
    public interface IMonoBox : IDisposable
    {
        string Name { get; }

        bool SupportsCpuLimit { get; }

        int? CpuLimit { get; set; }

        bool AddProcess(Process process);
    }
}