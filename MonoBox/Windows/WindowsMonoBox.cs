﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
    class WindowsMonoBox : IMonoBox
    {
        private IntPtr _jobPtr;
        private bool _disposed;
        private int? _cpuLimit;

        public WindowsMonoBox(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Cannot be null, empty or whitespace", "name");

            
            Name = name;
            _jobPtr = CreateJobObject(name);
            _cpuLimit = null;
        }

        private static IntPtr CreateJobObject(string name)
        {
            const bool inheritSecurityHandle = false;
            var securityDescriptor = (IntPtr) null;
            var securityAttributes = new SECURITY_ATTRIBUTES
                                         {
                                             bInheritHandle = inheritSecurityHandle ? 1 : 0,
                                             lpSecurityDescriptor = securityDescriptor
                                         };
            securityAttributes.nLength = Marshal.SizeOf(securityAttributes);
            return Kernel32.CreateJobObject(ref securityAttributes, name);
        }

        public string Name
        {
            get; private set;
        }

        public bool SupportsCpuLimit
        {
            get { return true; }
        }

        public int? CpuLimit
        {
            get
            {
                return _cpuLimit;
            }
            set
            {
                if (!value.HasValue)
                    return;

                if (value < 0 || value > 100)
                    throw new ArgumentOutOfRangeException("value", "CpuLimit must be between 0 and 100");

                SetHardCapCpuLimit(value.Value);
                GetHardCapCpuLimit();
                _cpuLimit = value.Value;
            }
        }

        private void GetHardCapCpuLimit()
        {
            var value = new JOBOBJECT_CPU_RATE_CONTROL_INFORMATION();
            int size;
            var querySuccess = Kernel32.QueryInformationJobObject(_jobPtr, JobObjectInfoClass.CpuRateControlInformation, out value,
                                                           Marshal.SizeOf(value), out size);
            if (!querySuccess)
                throw new Exception(string.Format("Unable to get information.  Error: {0}", Marshal.GetLastWin32Error()));
        }

        private void SetHardCapCpuLimit(int percentage)
        {
            var value = new JOBOBJECT_CPU_RATE_CONTROL_INFORMATION
                            {
                                ControlFlags = (uint) CpuRateControlFlags.JOB_OBJECT_CPU_RATE_CONTROL_HARD_CAP,
                                CpuRate = (uint) percentage
                            };

            var success = Kernel32.SetInformationJobObject(_jobPtr, JobObjectInfoClass.CpuRateControlInformation, ref value,
                                                     Marshal.SizeOf(value));
            if (!success)
                throw new Exception(string.Format("Unable to set information.  Error: {0}", Marshal.GetLastWin32Error()));
        }

        public bool AddProcess(Process process)
        {
            return Kernel32.AssignProcessToJobObject(_jobPtr, process.Handle);
        }

        public void Close()
        {
            Kernel32.CloseHandle(_jobPtr);
            _jobPtr = IntPtr.Zero;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
            }

            Close();
            _disposed = true;
        }
    }
}