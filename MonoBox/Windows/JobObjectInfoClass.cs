﻿namespace MonoBox.Windows
{
	public enum JobObjectInfoClass
	{
        BasicAccountingInformation = 1,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_BASIC_LIMIT_INFORMATION structure
        /// </summary>
        BasicLimitInformation = 2,

        BasicProcessIdList = 3,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_BASIC_UI_RESTRICTIONS structure
        /// </summary>
        BasicUIRestrictions = 4,
        
		/// <summary>
		/// This flag is not supported. Applications must set security limitations individually for each process.
		/// </summary>
		SecurityLimitInformation = 5,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_END_OF_JOB_TIME_INFORMATION structure
        /// </summary>
        EndOfJobTimeInformation = 6,
        
        /// <summary>
		/// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_ASSOCIATE_COMPLETION_PORT structure
		/// </summary>
		AssociateCompletionPortInformation = 7,

        BasicAndIoAccountingInformation = 8,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure
        /// </summary>
        ExtendedLimitInformation = 9,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a USHORT value that specifies the list of processor groups to assign the job to. The cbJobObjectInfoLength parameter is set to the size of the group data. Divide this value by sizeof(USHORT) to determine the number of groups
        /// </summary>
        GroupInformation = 11,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_NOTIFICATION_LIMIT_INFORMATION structure.
        /// </summary>
        NotificationLimitInformation = 12,

        /// <summary>
        /// The lpJobObjectInfo parameter is a pointer to a buffer that contains an array of GROUP_AFFINITY structures that specify the affinity of the job for the processor groups to which the job is currently assigned. The cbJobObjectInfoLength parameter is set to the size of the group affinity data. Divide this value by sizeof(GROUP_AFFINITY) to determine the number of groups.
        /// </summary>
        GroupInformationEx = 14,

		/// <summary>
		/// The lpJobObjectInfo parameter is a pointer to a JOBOBJECT_CPU_RATE_CONTROL_INFORMATION structure
		/// </summary>
		CpuRateControlInformation = 15
	}
}