﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
	static class Kernel32
	{
		/// <summary>
		/// Creates a job object
		/// http://msdn.microsoft.com/en-us/library/windows/desktop/ms682409(v=vs.85).aspx
		/// </summary>
		/// <param name="lpJobAttributes">
		/// A pointer to a SECURITY_ATTRIBUTES structure that specifies the security descriptor for the job object and determines whether child processes can inherit the returned handle. 
		/// If lpJobAttributes is NULL, the job object gets a default security descriptor and the handle cannot be inherited. 
		/// The ACLs in the default security descriptor for a job object come from the primary or impersonation token of the creator.
		/// </param>
		/// <param name="lpName">
		/// The name of the job.
		/// The name is limited to MAX_PATH characters. 
		/// Name comparison is case-sensitive.  
		/// If lpName is NULL, the job is created without a name.  
		/// If lpName matches the name of an existing event, semaphore, mutex, waitable timer, or file-mapping object, the function fails and the GetLastError function returns ERROR_INVALID_HANDLE. 
		/// This occurs because these objects share the same namespace
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is a handle to the job object. 
		/// The handle has the JOB_OBJECT_ALL_ACCESS access right. 
		/// If the object existed before the function call, the function returns a handle to the existing job object and GetLastError returns ERROR_ALREADY_EXISTS.  
		/// If the function fails, the return value is NULL. 
		/// To get extended error information, call GetLastError.
		/// </returns>
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern IntPtr CreateJobObject([In] ref SECURITY_ATTRIBUTES lpJobAttributes, string lpName);

        /// <summary>
        /// Opens an existing job object.
        /// </summary>
        /// <param name="dwDesiredAccess">
        /// The access to the job object. 
        /// This parameter can be one or more of the job object access rights. 
        /// This access right is checked against any security descriptor for the object.
        /// </param>
        /// <param name="bInheritHandles">
        /// If this value is TRUE, processes created by this process will inherit the handle. Otherwise, the processes do not inherit this handle.
        /// </param>
        /// <param name="lpName">
        /// The name of the job to be opened. 
        /// Name comparisons are case sensitive.
        /// This function can open objects in a private namespace. For more information, see Object Namespaces.
        /// </param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        internal static extern IntPtr OpenJobObject(uint dwDesiredAccess, bool bInheritHandle, string lpName);

        /// <summary>
        /// Terminates all processes currently associated with the job. 
        /// If the job is nested, this function terminates all processes currently associated with the job and all of its child jobs in the hierarchy.
        /// </summary>
        /// <param name="hJob">
        /// A handle to the job whose processes will be terminated. The CreateJobObject or OpenJobObject function returns this handle. 
        /// This handle must have the JOB_OBJECT_TERMINATE access right. For more information, see Job Object Security and Access Rights.
        /// The handle for each process in the job object must have the PROCESS_TERMINATE access right. 
        /// For more information, see Process Security and Access Rights.
        /// </param>
        /// <param name="uExitCode">
        /// The exit code to be used by all processes and threads in the job object. 
        /// Use the GetExitCodeProcess function to retrieve each process's exit value.
        /// Use the GetExitCodeThread function to retrieve each thread's exit value.
        /// </param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        internal static extern bool TerminateJobObject(IntPtr hJob, uint uExitCode);

        /// <summary>
		/// Sets limits for a job object
		/// http://msdn.microsoft.com/en-us/library/windows/desktop/ms686216(v=vs.85).aspx
		/// </summary>
		/// <param name="hJob">
		/// A handle to the job whose limits are being set. 
		/// The CreateJobObject or OpenJobObject function returns this handle. 
		/// The handle must have the JOB_OBJECT_SET_ATTRIBUTES access right. For more information, see Job Object Security and Access Rights.
		/// </param>
		/// <param name="infoType">
		/// The information class for the limits to be set.
		/// </param>
		/// <param name="lpJobObjectInfo">
		/// The limits or job state to be set for the job. The format of this data depends on the value of JobObjectInfoClass.
		/// </param>
		/// <param name="cbJobObjectInfoLength">
		/// The size of the job information being set, in bytes.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. 
		/// To get extended error information, call GetLastError.
		/// </returns>
		/// <remarks>
		/// You can use the SetInformationJobObject function to set several limits in a single call. 
		/// If you want to establish the limits one at a time or change a subset of the limits, call the QueryInformationJobObject function to obtain the current limits, modify these limits, and then call SetInformationJobObject.
		/// You must set security limits individually for each process associated with a job object, rather than setting them for the job object itself. 
		/// For information, see http://msdn.microsoft.com/en-us/library/windows/desktop/ms684880(v=vs.85).aspx.
		/// </remarks>
        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "SetInformationJobObject")]
        internal static extern bool SetInformationJobObject(IntPtr hJob, JobObjectInfoClass infoType, ref JOBOBJECT_BASIC_LIMIT_INFORMATION lpJobObjectInfo, int cbJobObjectInfoLength);

        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "SetInformationJobObject")]
        internal static extern bool SetInformationJobObject(IntPtr hJob, JobObjectInfoClass infoType, ref JOBOBJECT_EXTENDED_LIMIT_INFORMATION lpJobObjectInfo, int cbJobObjectInfoLength);

        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "SetInformationJobObject")]
        internal static extern bool SetInformationJobObject(IntPtr hJob, JobObjectInfoClass infoType, ref JOBOBJECT_CPU_RATE_CONTROL_INFORMATION lpJobObjectInfo, int cbJobObjectInfoLength);

        /// <summary>
        /// Retrieves limit and job state information from the job object.
        /// </summary>
        /// <param name="hJob">
        /// A handle to the job whose information is being queried. 
        /// The CreateJobObject or OpenJobObject function returns this handle. 
        /// The handle must have the JOB_OBJECT_QUERY access right. 
        /// For more information, see Job Object Security and Access Rights.
        /// If this value is NULL and the calling process is associated with a job, the job associated with the calling process is used. 
        /// If the job is nested, the immediate job of the calling process is used.
        /// </param>
        /// <param name="jobObjectInformationClass">
        /// The information class for the limits to be queried. 
        /// This parameter can be one of the following values.
        /// </param>
        /// <param name="lpJobObjectInfo">
        /// The limit or job state information. 
        /// The format of this data depends on the value of the JobObjectInfoClass parameter.
        /// </param>
        /// <param name="cbJobObjectInfoLength">
        /// The count of the job information being queried, in bytes. 
        /// This value depends on the value of the JobObjectInfoClass parameter.
        /// </param>
        /// <param name="lpReturnLength">
        /// A pointer to a variable that receives the length of data written to the structure pointed to by the lpJobObjectInfo parameter. 
        /// If you do not want to receive this information, specify NULL.
        /// </param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "QueryInformationJobObject")]
        internal static extern bool QueryInformationJobObject(IntPtr hJob, JobObjectInfoClass jobObjectInformationClass, out JOBOBJECT_BASIC_LIMIT_INFORMATION lpJobObjectInfo, int cbJobObjectInfoLength, out int lpReturnLength);

        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "QueryInformationJobObject")]
        internal static extern bool QueryInformationJobObject(IntPtr hJob, JobObjectInfoClass jobObjectInformationClass, out JOBOBJECT_EXTENDED_LIMIT_INFORMATION lpJobObjectInfo, int cbJobObjectInfoLength, out int lpReturnLength);

        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "QueryInformationJobObject")]
        internal static extern bool QueryInformationJobObject(IntPtr hJob, JobObjectInfoClass jobObjectInformationClass, out JOBOBJECT_CPU_RATE_CONTROL_INFORMATION lpJobObjectInfo, int cbJobObjectInfoLength, out int lpReturnLength);

        /// <summary>
		/// Assigns a process to an existing job object.
		/// http://msdn.microsoft.com/en-us/library/windows/desktop/ms681949(v=vs.85).aspx
		/// </summary>
		/// <param name="job">
		/// A handle to the job object to which the process will be associated. 
		/// The CreateJobObject or OpenJobObject function returns this handle. 
		/// The handle must have the JOB_OBJECT_ASSIGN_PROCESS access right. 
		/// For more information, see http://msdn.microsoft.com/en-us/library/windows/desktop/ms684164(v=vs.85).aspx.
		/// </param>
		/// <param name="process">
		/// A handle to the process to associate with the job object. 
		/// The handle must have the PROCESS_SET_QUOTA and PROCESS_TERMINATE access rights. 
		/// For more information, see Process Security and Access Rights.
		/// If the process is already associated with a job, the job specified by hJob must be empty or it must be in the hierarchy of nested jobs to which the process already belongs, and it cannot have UI limits set (SetInformationJobObject with JobObjectBasicUIRestrictions). 
		/// For more information, see Remarks.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. 
		/// To get extended error information, call GetLastError.
		/// </returns>
		/// <remarks>
		/// More information at
		/// http://msdn.microsoft.com/en-us/library/windows/desktop/ms681949(v=vs.85).aspx
		/// </remarks>
		[DllImport("kernel32.dll", SetLastError = true)]
		internal static extern bool AssignProcessToJobObject(IntPtr job, IntPtr process);

		/// <summary>
		/// Closes an open object handle.
		/// http://msdn.microsoft.com/en-us/library/ms724211(v=vs.85).aspx
		/// </summary>
		/// <param name="hObject">
		/// A valid handle to an open object.
		/// </param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// If the function fails, the return value is zero. 
		/// To get extended error information, call GetLastError.
		/// If the application is running under a debugger, the function will throw an exception if it receives either a handle value that is not valid or a pseudo-handle value. 
		/// This can happen if you close a handle twice, or if you call CloseHandle on a handle returned by the FindFirstFile function instead of calling the FindClose function.
		/// </returns>
		/// <remarks>
		/// More information at
		/// http://msdn.microsoft.com/en-us/library/ms724211(v=vs.85).aspx
		/// </remarks>
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		internal static extern int CloseHandle(IntPtr hObject);
		
		/// <summary>
		/// Retrieves the identifier of the thread that created the specified window and, optionally, the identifier of the process that created the window.
		/// http://msdn.microsoft.com/en-us/library/windows/desktop/ms633522(v=vs.85).aspx
		/// </summary>
		/// <param name="hWnd">
		/// A handle to the window.
		/// </param>
		/// <param name="lpdwProcessId">
		/// A pointer to a variable that receives the process identifier. 
		/// If this parameter is not NULL, GetWindowThreadProcessId copies the identifier of the process to the variable; otherwise, it does not.
		/// </param>
		/// <returns></returns>
		[DllImport("user32.dll", SetLastError = true)]
		public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
	}
}