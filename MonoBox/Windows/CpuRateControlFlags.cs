﻿namespace MonoBox.Windows
{
    enum CpuRateControlFlags
    {
        JOB_OBJECT_CPU_RATE_CONTROL_WEIGHT_BASED = 2,
        JOB_OBJECT_CPU_RATE_CONTROL_HARD_CAP = 4,
        JOB_OBJECT_CPU_RATE_CONTROL_NOTIFY = 8
    }
}