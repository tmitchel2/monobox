﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
    public class NewJobObject : IDisposable
    {
        private IntPtr? _jobPtr;
        private bool _disposed;
        private int? _cpuLimit;
        private IntPtr _securityDescriptor;

        public NewJobObject(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Cannot be null, empty or whitespace", "name");

            var inheritSecurityHandle = false;
            _securityDescriptor = (IntPtr) null;
            var securityAttributes = new SECURITY_ATTRIBUTES();
            securityAttributes.bInheritHandle = inheritSecurityHandle ? 1 : 0;
            securityAttributes.lpSecurityDescriptor = _securityDescriptor;
            securityAttributes.nLength = Marshal.SizeOf(securityAttributes);

            Name = name;
            _jobPtr = Kernel32.CreateJobObject(ref securityAttributes, name);
        }

        public string Name { get; private set; }

        public TimeSpan? PerProcessUserTimeLimit { get; set; }

        public TimeSpan? PerJobUserTimeLimit { get; set; }

        public long? MinimumWorkingSetSize { get; set; }

        public long? MaximumWorkingSetSize { get; set; }

        public int? ActiveProcessLimit { get; set; }

        public int? CpuLimit
        {
            get { return _cpuLimit; }
            set
            {
                if (!value.HasValue)
                    return;

                if (value < 0 || value > 100)
                    throw new ArgumentOutOfRangeException("value", "CpuLimit must be between 0 and 100");

                var basicInfo = GetBasicLimitInformation();
                var extendedInfo = GetExtendedLimitInformation();

                extendedInfo.BasicLimitInformation.LimitFlags = LimitFlags.JOB_OBJECT_LIMIT_ACTIVE_PROCESS | LimitFlags.JOB_OBJECT_LIMIT_PROCESS_TIME;
                extendedInfo.BasicLimitInformation.ActiveProcessLimit = 1;
                extendedInfo.BasicLimitInformation.PerProcessUserTimeLimit = TimeSpan.FromSeconds(10).Ticks;
                SetExtendedLimitInformation(extendedInfo);
                extendedInfo = GetExtendedLimitInformation();
                
                //var basicUIRestrictions = GetBasicUIRestrictions();
                //var queryInfo = GetCpuRateControlInformation();

                _cpuLimit = value;
            }
        }

        private JOBOBJECT_BASIC_LIMIT_INFORMATION GetBasicLimitInformation()
        {
            var queryInfo = new JOBOBJECT_BASIC_LIMIT_INFORMATION();
            int size;
            var querySuccess = Kernel32.QueryInformationJobObject(_jobPtr.Value, JobObjectInfoClass.BasicLimitInformation, out queryInfo,
                                               Marshal.SizeOf(queryInfo), out size);
            if (!querySuccess)
                throw new Exception(string.Format("Unable to get information.  Error: {0}", Marshal.GetLastWin32Error()));

            return queryInfo;
        }

        private JOBOBJECT_EXTENDED_LIMIT_INFORMATION GetExtendedLimitInformation()
        {
            var queryInfo = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION
            {
                BasicLimitInformation = new JOBOBJECT_BASIC_LIMIT_INFORMATION()
            };
            int size;

            var querySuccess = Kernel32.QueryInformationJobObject(_jobPtr.Value, JobObjectInfoClass.ExtendedLimitInformation, out queryInfo,
                                               Marshal.SizeOf(queryInfo), out size);
            if (!querySuccess)
                throw new Exception(string.Format("Unable to get information.  Error: {0}", Marshal.GetLastWin32Error()));

            return queryInfo;
        }

        private void SetBasicLimitInformation(JOBOBJECT_BASIC_LIMIT_INFORMATION value)
        {
            var success = Kernel32.SetInformationJobObject(_jobPtr.Value, JobObjectInfoClass.ExtendedLimitInformation, ref value,
                                                     Marshal.SizeOf(value));
            if (!success)
                throw new Exception(string.Format("Unable to set information.  Error: {0}", Marshal.GetLastWin32Error()));
        }

        private void SetExtendedLimitInformation(JOBOBJECT_EXTENDED_LIMIT_INFORMATION value)
        {
            var success = Kernel32.SetInformationJobObject(_jobPtr.Value, JobObjectInfoClass.ExtendedLimitInformation, ref value,
                                               Marshal.SizeOf(value));
            if (!success)
                throw new Exception(string.Format("Unable to set information.  Error: {0}", Marshal.GetLastWin32Error()));
        }
        
        private JOBOBJECT_CPU_RATE_CONTROL_INFORMATION GetCpuRateControlInformation()
        {
            //var queryInfo = new JOBOBJECT_CPU_RATE_CONTROL_INFORMATION();
            //var queryInfoSize = Marshal.SizeOf(queryInfo);
            //var queryInfoPtr = Marshal.AllocHGlobal(queryInfoSize);
            //Marshal.StructureToPtr(queryInfo, queryInfoPtr, false);
            //var returnLengthPtr = new IntPtr();
            //var querySuccess = Kernel32.QueryInformationJobObject(_jobPtr.Value, JobObjectInfoClass.CpuRateControlInformation, out queryInfoPtr,
            //                                   (uint)queryInfoSize, out returnLengthPtr);
            //if (!querySuccess)
            //    throw new Exception(string.Format("Unable to get information.  Error: {0}", Marshal.GetLastWin32Error()));

            //return queryInfo;
            throw new NotImplementedException();
        }

        private JOBOBJECT_BASIC_UI_RESTRICTIONS GetBasicUIRestrictions()
        {
            //var queryInfo = new JOBOBJECT_BASIC_UI_RESTRICTIONS();
            //var queryInfoSize = Marshal.SizeOf(queryInfo);
            //var queryInfoPtr = Marshal.AllocHGlobal(queryInfoSize);
            //Marshal.StructureToPtr(queryInfo, queryInfoPtr, false);
            //var returnLengthPtr = new IntPtr();
            //var querySuccess = Kernel32.QueryInformationJobObject(_jobPtr.Value, JobObjectInfoClass.BasicUIRestrictions, out queryInfoPtr,
            //                                   (uint)queryInfoSize, out returnLengthPtr);
            //if (!querySuccess)
            //    throw new Exception(string.Format("Unable to get information.  Error: {0}", Marshal.GetLastWin32Error()));

            //return queryInfo;
            throw new NotImplementedException();
        }

        private void SetCpuRateControlInformation(JOBOBJECT_CPU_RATE_CONTROL_INFORMATION value)
        {
            //var size = Marshal.SizeOf(value);
            //var ptr = Marshal.AllocHGlobal(size);
            //Marshal.StructureToPtr(value, ptr, false);
            //var success = Kernel32.SetInformationJobObject(_jobPtr.Value, JobObjectInfoClass.CpuRateControlInformation, ptr,
            //                                   (uint)size);
            //if (!success)
            //    throw new Exception(string.Format("Unable to set information.  Error: {0}", Marshal.GetLastWin32Error()));
         }


        public bool AddProcess(Process process)
        {
            return Kernel32.AssignProcessToJobObject(_jobPtr.Value, process.Handle);
        } 

        public void Close()
        {
            Kernel32.CloseHandle(_jobPtr.Value);
            _jobPtr = IntPtr.Zero;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
            }

            Close();
            _disposed = true;
        }

        /// <summary>
        /// Gets the last error.
        /// </summary>
        /// <returns></returns>
        private static int GetLastError()
        {
            return Marshal.GetLastWin32Error();
        }

        /// <summary>
        /// Gets the last error throw.
        /// </summary>
        private static void GetLastErrorThrow()
        {
            throw new Win32Exception(GetLastError());
        }
    }

    //public class JobObject : IDisposable
    //{
    //    private IntPtr _handle;
    //    private bool _disposed;

    //    public JobObject()
    //    {

    //        _handle = JobObjectMarshal.CreateJobObject(null, null);

    //        var info = new JOBOBJECT_BASIC_LIMIT_INFORMATION
    //                    {
    //                        LimitFlags = (short) LimitFlags.JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE
    //                    };
    //        var extendedInfo = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION
    //                            {
    //                                BasicLimitInformation = info
    //                            };

    //        int length = Marshal.SizeOf(typeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
    //        var extendedInfoPtr = Marshal.AllocHGlobal(length);
    //        Marshal.StructureToPtr(extendedInfo, extendedInfoPtr, false);

    //        var success = JobObjectMarshal.SetInformationJobObject(_handle, JOBOBJECTINFOCLASS.ExtendedLimitInformation,
    //                                                              extendedInfoPtr, (uint) length);
    //        if (!success)
    //            throw new Exception(string.Format("Unable to set information.  Error: {0}", Marshal.GetLastWin32Error()));
    //    }
		
    //    public bool AddProcess(Process process)
    //    {
    //        return AddProcess(process.Handle);
    //    }

    //    public bool AddProcess(IntPtr handle)
    //    {
    //        return JobObjectMarshal.AssignProcessToJobObject(_handle, handle);
    //    }

    //    public void Close()
    //    {
    //        JobObjectMarshal.CloseHandle(_handle);
    //        _handle = IntPtr.Zero;
    //    }

    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }

    //    private void Dispose(bool disposing)
    //    {
    //        if (_disposed)
    //            return;

    //        if (disposing)
    //        {
    //        }

    //        Close();
    //        _disposed = true;
    //    }
    //}
}
