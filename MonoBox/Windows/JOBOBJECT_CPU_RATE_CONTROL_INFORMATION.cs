﻿using System;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
    [StructLayout(LayoutKind.Explicit)]
    [CLSCompliant(false)]
    struct JOBOBJECT_CPU_RATE_CONTROL_INFORMATION
    {
        [FieldOffset(0)]
        public UInt32 ControlFlags;
        [FieldOffset(4)]
        public UInt32 CpuRate;
        [FieldOffset(4)]
        public UInt32 Weight;
    }
}