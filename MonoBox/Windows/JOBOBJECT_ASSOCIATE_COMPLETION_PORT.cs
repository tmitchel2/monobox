﻿using System;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
	/// <summary>
	/// Contains information used to associate a completion port with a job. 
	/// You can associate one completion port with a job. 
	/// There is no way to terminate the association and no way to associate a different port with the job.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	struct JOBOBJECT_ASSOCIATE_COMPLETION_PORT 
	{
		public IntPtr CompletionKey;
		public IntPtr CompletionPort;
	}
}