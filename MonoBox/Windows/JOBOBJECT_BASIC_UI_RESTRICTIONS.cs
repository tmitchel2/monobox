﻿using System;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
	/// <summary>
	/// Contains basic user-interface restrictions for a job object
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	struct JOBOBJECT_BASIC_UI_RESTRICTIONS 
	{
		public Int16 UIRestrictionsClass;
	}
}