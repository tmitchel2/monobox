﻿using System;
using System.Runtime.InteropServices;

namespace MonoBox.Windows
{
	/// <summary>
	/// Specifies the action the system will perform when an end-of-job time limit is exceeded
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	struct JOBOBJECT_END_OF_JOB_TIME_INFORMATION 
	{
		public Int16 EndOfJobTimeAction;
	}
}