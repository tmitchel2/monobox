﻿namespace MonoBox
{
    public interface IMonoBoxFactory
    {
        IMonoBox CreateMonoBox(string name);
    }
}