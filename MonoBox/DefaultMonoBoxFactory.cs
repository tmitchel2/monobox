﻿using MonoBox.Windows;

namespace MonoBox
{
    public class DefaultMonoBoxFactory : IMonoBoxFactory
    {
        public static IMonoBoxFactory Instance = new DefaultMonoBoxFactory();

        public IMonoBox CreateMonoBox(string name)
        {
            return new WindowsMonoBox(name);
        }
    }
}
