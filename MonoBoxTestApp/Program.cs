﻿using System;
using System.Threading;

namespace MonoBoxTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var startTime = DateTime.Now;
            // Use progressively more cpu time
            while (true)
            {
                for (var i = 0; i < 100000000; i++)
                {
                    var calc = 10000 * 10000;
                    calc = calc * 2 * 2;
                }
                var runTime = Convert.ToInt32((DateTime.Now - startTime).TotalSeconds) * 100;
                var sleep = 1000 - runTime;
                if (sleep < 0)
                    sleep = 0;
                Thread.Sleep(sleep);
            }
        }
    }
}
